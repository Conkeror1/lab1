﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public struct Time
    {
        public int H { get; }
        public int M { get; }
        public Time(int hours, int minutes)
        {
            H = hours;
            M = minutes;
        }

        public override string ToString()
        {
            return $"{H:00}:{M:00}";
        }

    }
}
