﻿using System;

namespace Lab1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Laboratory work #1. Git");
            Console.WriteLine("Variant #2. Conference program");
            Console.WriteLine("Author: Nikita Danikovich");
            Console.WriteLine("Group: 22POIT1z");

            Handler pr = new Handler();
            pr.ReadFile(@"..\..\..\Data.txt");
            pr.Print(pr.Confs);

            Console.WriteLine(pr.MaxDuration());

            pr.Print(pr.ReportsByPerson());
            pr.Print(pr.ReportsByTime());

            pr.Print(pr.SortListWithPyramidSort(pr.Confs, "длительность"));
            pr.Print(pr.SortListWithPyramidSort(pr.Confs, "фамилия"));

            pr.Print(pr.SortListWithMergeSort(pr.Confs, "длительность"));
            pr.Print(pr.SortListWithMergeSort(pr.Confs, "фамилия"));


        }
    }
}
