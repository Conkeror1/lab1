﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public struct Person
    {
        public string FirstName { get; }
        public string SecondName { get; }
        public string LastName { get; }

        public Person(string firstName, string secondName, string lastName)
        {
            FirstName = firstName;
            SecondName = secondName;
            LastName = lastName;
        }

        public override string ToString()
        {
            return $"{LastName} {FirstName} {SecondName}";
        }

    }
}
