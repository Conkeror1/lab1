﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class Conf
    {
        public Time StartTime { get; }
        public Time EndTime { get; }
        public Person Person { get; }
        public string Topic { get; }
        public int Duration { get; }

        public Conf(Time startTime, Time endTime, Person person, string topic)
        {
            StartTime = startTime;
            EndTime = endTime;
            Person = person;
            Topic = topic;
            Duration = endTime.H * 60 + endTime.M - startTime.H * 60 - startTime.M;
        }

        public override string ToString()
        {
            return $"{StartTime} {EndTime} {Person} {Topic}";
        }

    }
}
