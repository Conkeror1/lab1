﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class Handler
    {
        public List<Conf> Confs { get; }

        public Handler()
        {
            Confs = new List<Conf>();
        }

        public void ReadFile(string filePath)
        {
            using (var sr = new StreamReader(filePath))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    string[] items = line.Split(" ");
                    string[] time = items[0].Split(":");
                    Time startTime = new Time(Convert.ToInt32(time[0]), Convert.ToInt32(time[1]));
                    time = items[1].Split(":");
                    Time endTime = new Time(Convert.ToInt32(time[0]), Convert.ToInt32(time[1]));
                    Person person = new Person(items[3], items[4], items[2]);
                    string topic = "";
                    for (int i = 5; i < items.Length; i++)
                    {
                        topic += items[i];
                        if (i != items.Length - 1)
                        {
                            topic += " ";
                        }
                    }

                    Confs.Add(new Conf(startTime, endTime, person, topic));
                }
            }
        }

        public void Print(List<Conf> Confs)
        {
            foreach (Conf con in Confs)
            {
                Console.WriteLine(con.ToString());
            }
            Console.WriteLine();
        }

        public Time MaxDuration()
        {
            int maxDuration = Confs[0].Duration;
            for (int i = 1; i < Confs.Count; i++)
            {
                if (Confs[i].Duration > maxDuration)
                {
                    maxDuration = Confs[i].Duration;
                }
            }

            return new Time(maxDuration / 60, maxDuration - maxDuration / 60 * 60);
        }

        public List<Conf> ReportsByPerson()
        {
            List<Conf> result = new List<Conf>();
            foreach (Conf con in Confs)
            {
                if (con.Person.ToString().Equals("Иванов Иван Иванович"))
                {
                    result.Add(con);
                }
            }

            return result;
        }

        public List<Conf> ReportsByTime()
        {
            List<Conf> result = new List<Conf>();
            foreach (Conf con in Confs)
            {
                if (con.Duration > 15)
                {
                    result.Add(con);
                }
            }

            return result;
        }

        public List<Conf> SortListWithPyramidSort(List<Conf> list, string sortvalue)
        {
            Conf[] arr = list.ToArray();
            switch (sortvalue)
            {
                case "длительность":
                    return PyramidSort(arr, arr.Length, "длительность").ToList();
                case "фамилия":
                    arr = PyramidSort(arr, arr.Length, "доклад");
                    return PyramidSort(arr, arr.Length, "фамилия").ToList();
                default: return arr.ToList();
            }
        }

        private int AddToPyramid(Conf[] list, int i, int N, string sortvalue)
        {
            int imax = 0;
            if ((2 * i + 2) < N)
            {
                switch (sortvalue)
                {
                    case "длительность":
                        if (list[2 * i + 1].Duration > list[2 * i + 2].Duration)
                        {
                            imax = 2 * i + 2;
                        }
                        else
                        {
                            imax = 2 * i + 1;
                        }

                        break;
                    case "фамилия":
                        if (list[2 * i + 1].Person.LastName.CompareTo(list[2 * i + 2].Person.LastName) < 0)
                        {
                            imax = 2 * i + 2;
                        }
                        else
                        {
                            imax = 2 * i + 1;
                        }

                        break;
                    case "доклад":
                        if (list[2 * i + 1].Topic.CompareTo(list[2 * i + 2].Topic) < 0)
                        {
                            imax = 2 * i + 2;
                        }
                        else
                        {
                            imax = 2 * i + 1;
                        }

                        break;
                }
            }
            else
            {
                imax = 2 * i + 1;
            }

            if (imax >= N)
            {
                return i;
            }

            switch (sortvalue)
            {
                case "длительность":
                    if (list[i].Duration > list[imax].Duration)
                    {
                        (list[i], list[imax]) = (list[imax], list[i]);
                        if (imax < N / 2)
                        {
                            i = imax;
                        }
                    }

                    break;
                case "фамилия":
                    if (list[i].Person.LastName.CompareTo(list[imax].Person.LastName) < 0)
                    {
                        (list[i], list[imax]) = (list[imax], list[i]);
                        if (imax < N / 2)
                        {
                            i = imax;
                        }
                    }

                    break;
                case "доклад":
                    if (list[i].Topic.CompareTo(list[imax].Topic) < 0)
                    {
                        (list[i], list[imax]) = (list[imax], list[i]);
                        if (imax < N / 2)
                        {
                            i = imax;
                        }
                    }

                    break;
            }

            return i;
        }

        private Conf[] PyramidSort(Conf[] list, int len, string sortvalue)
        {
            // построение пирамиды
            for (int i = len / 2 - 1; i >= 0; --i)
            {
                int prev_i = i;
                i = AddToPyramid(list, i, len, sortvalue);
                if (prev_i != i)
                {
                    ++i;
                }
            }

            // сортировка
            for (int k = len - 1; k > 0; --k)
            {
                (list[0], list[k]) = (list[k], list[0]);
                int i = 0, prev_i = -1;
                while (i != prev_i)
                {
                    prev_i = i;
                    i = AddToPyramid(list, i, k, sortvalue);
                }
            }

            return list;
        }

        public List<Conf> SortListWithMergeSort(List<Conf> list, string sortvalue)
        {
            switch (sortvalue)
            {
                case "длительность":
                    return SortListWithMergeSortByParameter(list, "длительность");
                case "фамилия":
                    list = SortListWithMergeSortByParameter(list, "доклад");
                    return SortListWithMergeSortByParameter(list, "фамилия");
                default: return list;
            }
        }

        private List<Conf> SortListWithMergeSortByParameter(List<Conf> list, string sortvalue)
        {
            if (list.Count <= 1)
            {
                return list;
            }

            var left = new List<Conf>();
            var right = new List<Conf>();
            int middle = list.Count / 2;
            for (int i = 0; i < middle; i++)
            {
                left.Add(list[i]);
            }

            for (int i = middle; i < list.Count; i++)
            {
                right.Add(list[i]);
            }

            left = SortListWithMergeSortByParameter(left, sortvalue);
            right = SortListWithMergeSortByParameter(right, sortvalue);
            switch (sortvalue)
            {
                case "длительность":
                    return MergeByDuration(left, right);
                case "доклад":
                    return MergeByTopic(left, right);
                case "фамилия":
                    return MergeByLastName(left, right);
                default: return list;
            }
        }

        private static List<Conf> MergeByDuration(List<Conf> left, List<Conf> right)
        {
            var result = new List<Conf>();
            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First().Duration <= right.First().Duration)
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                        continue;
                    }

                    result.Add(left.First());
                    left.Remove(left.First());
                    continue;
                }

                if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                    continue;
                }

                result.Add(right.First());
                right.Remove(right.First());
            }

            return result;
        }

        private static List<Conf> MergeByTopic(List<Conf> left, List<Conf> right)
        {
            var result = new List<Conf>();
            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First().Topic.CompareTo(right.First().Topic) <= 0)
                    {
                        result.Add(left.First());
                        left.Remove(left.First());
                        continue;
                    }

                    result.Add(right.First());
                    right.Remove(right.First());
                    continue;
                }

                if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                    continue;
                }

                result.Add(right.First());
                right.Remove(right.First());
            }

            return result;
        }

        private static List<Conf> MergeByLastName(List<Conf> left, List<Conf> right)
        {
            var result = new List<Conf>();
            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {
                    if (left.First().Person.LastName.CompareTo(right.First().Person.LastName) <= 0)
                    {
                        result.Add(left.First());
                        left.Remove(left.First());
                        continue;
                    }

                    result.Add(right.First());
                    right.Remove(right.First());
                    continue;
                }

                if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                    continue;
                }

                result.Add(right.First());
                right.Remove(right.First());
            }

            return result;
        }
    }
}
